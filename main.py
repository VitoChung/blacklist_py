from bs4 import BeautifulSoup
import requests

import pdfplumber
import pandas as pd
from pathlib import Path

headers = {'user-agent': 'Mozilla/5.0 (Macintosh Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/66.0.3359.181 Safari/537.36'}


def main():
    url = "https://www.trade.gov.tw/Pages/Detail.aspx?nodeID=242&pid=662639"
    res = requests.get(url, headers=headers, verify=False)
    soup = BeautifulSoup(res.text, 'html.parser')

    links = soup.find_all('a')
    for a in links:
        if '我國出口實體管理名單' in a.text.strip():
            file = a.attrs['href']
            get_pdf(file)


def get_pdf(file_url):
    response = requests.get(file_url, headers=headers, verify=False)
    filename = Path('d:/BlackList.pdf')
    filename.write_bytes(response.content)

    with pdfplumber.open('d:/BlackList.pdf') as pdf:
        for i in range(len(pdf.pages)):
            page = pdf.pages[i]
            table = page.extract_table()

            if i == 0:
                yaohao_df = pd.DataFrame(table)
            else:
                yaohao_df = yaohao_df.append(pd.DataFrame(table))

    yaohao_df.to_excel('d:/BlackList.xlsx', index=False, header=True)


if __name__ == '__main__':
    main()

